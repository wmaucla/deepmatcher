{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a part of the ***Magellan*** ecosystem, in this tutorial we will show how to use `deepmatcher` together with `Magellan`, to perform an end-to-end Entity Matching (EM) task. Specifically, `Magellan` performs EM in a **two-stage** fashion where given two tables,\n",
    "1. Perform **blocking** on the two tables by removing obvious non-matching tuple pairs to get a candidate set K.\n",
    "2. Perform **matching** to predict each pair in K as match or non-match. This stage consists of the following substeps:\n",
    "    1. *Magellan* first helps the user take a sample S from K.\n",
    "    2. The user labels all pairs in S as the training data.\n",
    "    3. A classifer L will be learned on S.\n",
    "    4. The user applies L to K to predict each pair as match or non-match.\n",
    "\n",
    "Given the above workflow, `deepmatcher` fits in the step C, which is to take advantage of deep learning to learn a classifer. For the rest of the tutorial, we will use a real example, which is to match songs across two tables from iTunes and Amazon Music, to go through the workflow.\n",
    "\n",
    "For more information on `Magellan`, please go to the project website: https://sites.google.com/site/anhaidgroup/projects/magellan."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 0. Preparation\n",
    "In order to use ***Magellan***, we need to first install it. The easiest way is to use \"pip\" as follow (please consult the package website for other installation options)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    import py_entitymatching as em\n",
    "except:\n",
    "    !pip install py_entitymatching"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.path.append('/Users/pradap/Documents/Research/Python-Package/anhaid/deepmatcher')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import deepmatcher as dm\n",
    "import py_entitymatching as em\n",
    "\n",
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 1. Load data\n",
    "We first load the two input tables in the csv format using `Magellan`, that contain songs from iTunes and Amazon Music. These two tables are in the \"example\" directory included in our package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The path to the two input tables.\n",
    "path_A = os.path.join('.', 'sample_data', 'itunes-amazon', 'tableA.csv')\n",
    "path_B = os.path.join('.', 'sample_data', 'itunes-amazon', 'tableB.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the two tables.\n",
    "A = em.read_csv_metadata(path_A, key='id')\n",
    "B = em.read_csv_metadata(path_B, key='id')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Basic information about the tables.\n",
    "print('Number of tuples in A: ' + str(len(A)))\n",
    "print('Number of tuples in B: ' + str(len(B)))\n",
    "print('Number of tuples in A X B (i.e the cartesian product): ' + str(len(A)*len(B)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The first few tuples in table A.\n",
    "A.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The first few tuples in table B.\n",
    "B.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 2. Block tables to get the candidate set\n",
    "We first perform blocking on A and B to get a candidate set K, by removing obvious non-matching tuple pairs. `Magellan` supports four different types of blocker: (1) attribute equivalence, (2) overlap, (3) rule-based, and (4) black-box. Typically, users need to mix and match these blockers with the debugging functionality provided in `Magellan` to get a good candidate set. (Developing a good blocker is not the focus of this tutorial. For more information on developing and debugging a blocker, please consult the user manual of `Magellan`.)\n",
    "\n",
    "Here we show an example of blocking. Observe that matching tuple pairs should have some common words in the album name, so we first create an overlap blocker on the attribute name \"Album_Name\" with threshold 2, to remove all pairs with word overlap less 2 in that attribute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an overlap blocker in Magellan and apply it to A and B to get the candidate set K1 which is in the format of \n",
    "# a dataframe. The \"l_out_attrs\" and \"r_out_attrs\" parameters indicate the columns that will be included in K1 from A\n",
    "# and B respectively.\n",
    "ob = em.OverlapBlocker()\n",
    "K1 = ob.block_tables(A, B, 'Album_Name', 'Album_Name',\n",
    "                    l_output_attrs=['Song_Name', 'Artist_Name', 'Album_Name', 'Genre', 'Price', 'CopyRight', 'Time', 'Released'], \n",
    "                    r_output_attrs=['Song_Name', 'Artist_Name', 'Album_Name', 'Genre', 'Price', 'CopyRight', 'Time', 'Released'],\n",
    "                    overlap_size=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The number of tuple pairs in K1.\n",
    "len(K1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# The first few tuple pairs in K1\n",
    "K1.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that K1 has more than 3 million pair in it, which is too large to considered for matching. So we create an overlap blocking with threshold 1 on the attribute \"Artist_Name\" for K1, to filter all pairs in K1 that don't share any word in \"Artist_Name\". And we get the candidate set K2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a new overlap blocker to remove pairs from K1 that have no common word in \"Artist_Name\".\n",
    "K2 = ob.block_candset(K1, 'Artist_Name', 'Artist_Name', overlap_size=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The number of tuple pairs in K2.\n",
    "len(K2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After the second blocking step, now we have a candidate set K2 with about 170K pairs. But we think it is still a bit larger to consider for matching. So we apply a third blocker, which is an overlap blocker on the attribute \"Song_Name\", to further reduce the size of the candidate set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Apply the third overlap blocker.\n",
    "K3 = ob.block_candset(K2, 'Song_Name', 'Song_Name', overlap_size=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The number of tuples pairs in K3.\n",
    "len(K3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a candidate set with 38K pairs which is reasonable, so we take K3 as the final candidate set. We save the candidate to the disk in the csv format for future reuse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_K = os.path.join('.', 'sample_data', 'itunes-amazon', 'e2e-tutorial', 'candidate.csv')\n",
    "K3.to_csv(path_K, index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Step 3. Match tuple pairs in the candidate set\n",
    "In this stage we will match tuple pairs in the candidate set to predict each of them as match or non-match. This is the part that `deepmatcher` will be involved. Specifically, it consists of the following steps:\n",
    "1. Take a sample S from the candidate set K, and label all pairs in S.\n",
    "2. Train a classifier L using S. Specifically, we will train a classifier using `deepmatcher`.\n",
    "3. Apply L to the candidiate set K."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample and label the candidate set\n",
    "We first take a random sample S from the candidate set K using `Magellan`. Here for example, we sample 500 pairs for K. Then we label each pair as match (enter 1) or non-match (enter 0) and use S as the training data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Take a sample of 500 pairs from the candidate set.\n",
    "S = em.sample_table(K3, 500)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Label the sample S in a GUI. Enter 1 for match and 0 for non-match.\n",
    "G = em.label_table(S, 'gold')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purposes of this tutorial, we will load in a pre-labeled dataset (of 539 tuple pairs) included in this package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The path to the labeled data file.\n",
    "path_G = os.path.join('.', 'sample_data', 'itunes-amazon', 'e2e-tutorial', 'gold.csv')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the labeled data into a dataframe.\n",
    "G = em.read_csv_metadata(path_G, \n",
    "                         key='_id',\n",
    "                         ltable=A, rtable=B, \n",
    "                         fk_ltable='ltable_id', fk_rtable='rtable_id')\n",
    "print('Number of labeled pairs:', len(G))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train a classifier using labeled data\n",
    "Once we have the labeled data, we use `deepmatcher` to train a classifier. The first thing we need to do is to split the data for training purpose. In this example, we split the labeled data into three parts: training, validation and test data, with the ratio of 3:1:1. (For now we only support spliting the labeled data into three parts train/valid/test, where the validation set is used for selecting the best model during the training epochs.) For the purpose of caching data and progressive training, we will first save the split parts to disk in the format of csv files, then load them back in. The cache file will be saved during the loading procedure. For subsequent training runs, the cache file will be used to save preprocessing time on the raw csv files, unless the csv files are modified (in this case, new cache file will be generated)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The directory where the data splits will be saved.\n",
    "split_path = os.path.join('.', 'sample_data', 'itunes-amazon', 'e2e-tutorial')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Split labeled data into train, valid, and test csv files to disk, with the split ratio of 3:1:1.\n",
    "dm.data.split(G, split_path, 'train.csv', 'valid.csv', 'test.csv',\n",
    "              [3, 1, 1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the training data files from the disk. Ignore the \"left_id\" and \"right_id\" \n",
    "# columns for data preprocessing.\n",
    "# The 'use_magellan_convention' parameter asks deepmatcher to use Magellan's \n",
    "# naming convention for the left and right table column name prefixes \n",
    "# (\"ltable_\", and \"rtable_\"), and also to consider \"_id\" as the ID column.\n",
    "train, validation, test = dm.data.process(\n",
    "    path=os.path.join('.', 'sample_data', 'itunes-amazon', 'e2e-tutorial'),\n",
    "    cache='train_cache.pth',\n",
    "    train='train.csv',\n",
    "    validation='valid.csv',\n",
    "    test='test.csv',\n",
    "    use_magellan_convention=True,\n",
    "    ignore_columns=('ltable_id', 'rtable_id'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After we get the training data, we can use `deepmatcher` to train a classifier. Here we train a hybrid model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a hybrid model.\n",
    "model = dm.MatchingModel(attr_summarizer='hybrid')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Train the hybrid model with 10 training epochs, batch size of 16, positive-to-negative \n",
    "# ratio to be 3. We save the best model (with the \n",
    "# highest F1 score on the validation set) to 'hybrid_model.pth'.\n",
    "model.run_train(\n",
    "    train,\n",
    "    validation,\n",
    "    epochs=10,\n",
    "    batch_size=16,\n",
    "    best_save_path='hybrid_model.pth',\n",
    "    pos_neg_ratio=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the trained classifier, we can evaluate the accuracy using the test data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate the accuracy on the test data.\n",
    "model.run_eval(test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Apply the trained classifier to the candidate set\n",
    "Now we have a trained classifer, we can apply it to the candidate set to predict each pair as match or non-match. To achieve this, we first load the trained model and the candidate set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the model.\n",
    "model = dm.MatchingModel(attr_summarizer='hybrid')\n",
    "model.load_state('hybrid_model.pth')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the candidate set. Note that the trained model is an input parameter as we need to trained \n",
    "# model for candidate set preprocessing.\n",
    "candidate = dm.data.process_unlabeled(\n",
    "    path=os.path.join('.', 'sample_data', 'itunes-amazon', 'e2e-tutorial', 'candidate.csv'),\n",
    "    trained_model=model,\n",
    "    ignore_columns=('ltable_id', 'rtable_id'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After we load the candidate set, now we make the predictions using the `run_prediction` function. The *output_attribute* argument indicates the columns that will be included in the prediction table. Here \n",
    "we use all of the columns in the candidate set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Predict the pairs in the candidate set and return a dataframe containing the pair id with \n",
    "# the score of being a match.\n",
    "predictions = model.run_prediction(candidate, output_attributes=list(candidate.get_raw_table().columns))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below shows a few pairs in the prediction table. Note that this table is not sorted in the descending order of the \n",
    "match scores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "predictions.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can simply manipulate the prediction table above to get the pairs that we want. Suppose we are only interested in pairs with the match score more over 0.9, and we want to display them sorting in the descending order on match score. This can be achieved as follow."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "high_score_pairs = predictions[predictions['match_score'] >= 0.9].sort_values(by=['match_score'], ascending=False)\n",
    "high_score_pairs.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you have seen, `deepmatcher` does not include a column indicating match / non-match predictions in the output table. You can easily create such a column by thresholding the `match_score` at 0.5 (which is what `deepmatcher` uses as the threshold to compute F1), or using a threshold value or your choice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "predictions['match_prediction'] = predictions['match_score'].apply(lambda score: 1 if score >= 0.5 else 0)\n",
    "\n",
    "# Reorder columns to avoid scrolling...\n",
    "predictions =  predictions[['match_score', 'match_prediction'] + predictions.columns.values[1:-1].tolist()]\n",
    "predictions.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reset index as Magellan requires the key to be a column in the table\n",
    "predictions.reset_index(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Update metadata in the catalog. This information can later be used by triggers to modify the labels from \n",
    "# the learning-based matcher \n",
    "em.set_key(predictions, '_id')\n",
    "em.set_fk_ltable(predictions, 'ltable_id')\n",
    "em.set_fk_rtable(predictions, 'rtable_id')\n",
    "em.set_ltable(predictions, A)\n",
    "em.set_rtable(predictions, B)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
